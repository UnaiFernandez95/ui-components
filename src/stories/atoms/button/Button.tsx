import { CSSProperties, FC, StyleHTMLAttributes } from "react";
import "./button.css";

export type ButtonProps = {
  readonly backgroundColor: CSSProperties | undefined;
  readonly primary?: boolean;
  readonly size?: string;
  readonly onClick: () => void | undefined;
  readonly label?: string;
};

export const Button: FC<ButtonProps> = ({
  primary = false,
  backgroundColor,
  size = "medium",
  label,
  ...props
}: ButtonProps) => {
  const mode = primary
    ? "storybook-button--primary"
    : "storybook-button--secondary";
  return (
    <button
      type="button"
      className={["storybook-button", `storybook-button--${size}`, mode].join(
        " "
      )}
      style={backgroundColor}
      {...props}
    >
      {label}
    </button>
  );
};
