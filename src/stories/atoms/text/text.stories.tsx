import { Story, Meta } from "@storybook/react/types-6-0";
import Text, { TextProps, TextVariant } from "./Text";

export default {
  title: "atoms/Text",
  component: Text,
} as Meta<TextProps>;

const Template: Story<TextProps> = (args) => <Text {...args} />;

const Default = Template.bind({});
Default.args = { children: "lorem ipsum dolor" };

const Header1 = Template.bind({});
Header1.args = {
  variant: TextVariant.HEADING_1,
  children: "lorem ipsum dolor",
};

const Header2 = Template.bind({});
Header2.args = {
  variant: TextVariant.HEADING_2,
  children: "lorem ipsum dolor",
};

const Header3 = Template.bind({});
Header3.args = {
  variant: TextVariant.HEADING_3,
  children: "lorem ipsum dolor",
};

const Body = Template.bind({});
Body.args = {
  variant: TextVariant.BODY,
  children: "lorem ipsum dolor",
};

const BodySmall = Template.bind({});
BodySmall.args = {
  variant: TextVariant.BODY_SMALL,
  children: "lorem ipsum dolor",
};

const BodyBold = Template.bind({});
BodyBold.args = {
  variant: TextVariant.BODY_BOLD,
  children: "lorem ipsum dolor",
};

const SmallCaps = Template.bind({});
SmallCaps.args = {
  variant: TextVariant.SMALL_CAPS,
  children: "lorem ipsum dolor",
};
const Link = Template.bind({});
Link.args = {
  variant: TextVariant.LINK,
  children: "lorem ipsum dolor",
};

export {
  Default,
  Header1,
  Header2,
  Header3,
  Body,
  BodySmall,
  BodyBold,
  SmallCaps,
  Link,
};
